set project_name EDA2
file mkdir $project_name
cd ./$project_name
create_project -force $project_name ./ -part xc7a35tcpg236-1
set_property target_language verilog [current_project]
set_property simulator_language verilog [current_project]

add_files -fileset sources_1 -norecurse -copy_to ./$project_name.srcs/sources_1/new ../src/clk_divider.v
add_files -fileset sources_1 -norecurse -copy_to ./$project_name.srcs/sources_1/new ../src/decoder.v
add_files -fileset sources_1 -norecurse -copy_to ./$project_name.srcs/sources_1/new ../src/display.v
add_files -fileset sources_1 -norecurse -copy_to ./$project_name.srcs/sources_1/new ../src/keyboard.v
add_files -fileset sources_1 -norecurse -copy_to ./$project_name.srcs/sources_1/new ../src/TOP.v

add_files -fileset sim_1 -norecurse -copy_to ./$project_name.srcs/sim_1/new ../src/TOP_sim.v
add_files -fileset sim_1 -norecurse -copy_to ./$project_name.srcs/sim_1/new ../src/TOP_sim_tb.v

add_files -fileset constrs_1 -norecurse -copy_to ./$project_name.srcs/constrs_1 ../src/TOP.xdc

update_compile_order -fileset sources_1
update_compile_order -fileset sim_1

launch_runs synth_1 -jobs 2
wait_on_run synth_1

launch_runs impl_1 -to_step write_bitstream -jobs 2
wait_on_run impl_1

close_project
cd ../ */
