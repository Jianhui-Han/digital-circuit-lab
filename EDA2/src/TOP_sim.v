`timescale 1ns / 1ps

module TOP_sim(
    input clk,
    input rst,
    input [3:0] rows,
    output [3:0] cols,
    output [6:0] segs,
    output [3:0] digs
    );
    
    parameter INIT = 0, START = 1, INPUT = 2, CHARGE = 3; // state definitions
    
    wire clk_100M, clk_1M; // clocks
    wire valid; // valid for keys
    wire [3:0] keys; // keys
    wire str, clr, cfm, num, num_nonzero;
    wire en_display; // enable display
    wire en_timer_action;   // enable timer for action
    wire en_timer_charge;   // enable timer for 
    
    reg [1:0] state; // state registers
    reg [3:0] coins1, coins0, mins1, mins0; // digits to display
    reg [23:0] timer_action;
    reg [23:0] timer_charge;
    reg charge_minus, charge_end, no_action; // controls for state transfer 
    
    integer NO_ACTION = 0.5 * 10_000_000;
    integer CHARGE_MINUS = 0.5 * 1_000_000;
    
//    assign clk_100M = clk;
    assign clk_1M = clk;
    assign str = valid & (keys == 4'ha);    // a for start
    assign clr = valid & (keys == 4'hb);    // b for clear
    assign cfm = valid & (keys == 4'hc);    // c for confirm
    assign num = valid & (keys <= 4'h9);    // regular number
    assign num_nonzero = num & (keys != 4'h0); // nonzero number
    assign en_display = ~(state == INIT);   // disable when INIT
    assign en_timer_action = state == START;    // enable when START
    assign en_timer_charge = state == CHARGE;   // enable when CHARGE
    
    // controlling state machine
    always @ (posedge clk_1M or posedge rst) begin
        if (rst) begin
            // reset state
            state <= INIT;
            // reset values
            coins1 <= 4'h0; coins0 <= 4'h0;
            mins1 <= 4'h0; mins0 <= 4'h0;
        end
        else begin
            case (state)
                INIT: begin 
                    if (str) begin
                        // state
                        state <= START;
                        // values
                        coins1 <= 4'h0; coins0 <= 4'h0;
                        mins1 <= 4'h0; mins0 <= 4'h0;
                    end // INIT + str
                end // INIT
                START: begin
                    if (num_nonzero) begin
                        // next state
                        state <= INPUT;
                        // values
                        coins1 <= 4'h0; coins0 <= keys;
                        if (keys >= 5) begin
                            mins1 <= 4'h1; mins0 <= keys * 2 - 4'ha;
                        end
                        else begin
                            mins1 <= 4'h0; mins0 <= keys * 2;
                        end
                    end // START + num_nozero
                    else if (no_action) begin
                        // next state
                        state <= INIT;
                        // values do not matter
                        // coins1 <= 4'h0; coins0 <= 4'h0;
                        // mins1 <= 4'h0; mins1 <= 4'h0;
                    end // START + no_action
                end // START
                INPUT: begin
                    if (clr) begin
                        // next state 
                        state <= START;
                        // values
                        coins1 <= 4'h0; coins0 <= 4'h0;
                        mins1 <= 4'h0; mins0 <= 4'h0;
                    end // INPUT + clr
                    else if (num) begin
                        // next state
                        // state <= INPUT
                        // values
                        if (coins0 >= 4'h3) begin
                            coins1 <= 4'h2; coins0 <= 4'h0;
                            mins1 <= 4'h4; mins0 = 4'h0;
                        end
                        else if (num_nonzero && coins0 == 4'h2) begin
                            coins1 <= 4'h2; coins0 <= 4'h0;
                            mins1 <= 4'h4; mins0 = 4'h0;
                        end
                        else begin
                            coins1 <= coins0; coins0 <= keys;
                            if (coins0 == 4'h2) begin
                                mins1 <= 4'h4; mins0 = 4'h0;
                            end
                            else if (coins0 == 4'h1) begin
                                if (keys >= 4'h5) begin
                                    mins1 = 4'h3; mins0 = keys * 2 - 4'ha;
                                end
                                else begin
                                    mins1 = 4'h2; mins0 = keys * 2;
                                end
                            end
                            else begin
                                if (keys >= 4'h5) begin
                                    mins1 = 4'h1; mins0 = keys * 2 - 4'ha;
                                end
                                else begin
                                    mins1 = 4'h0; mins0 = keys * 2;
                                end
                            end
                        end
                    end // INPUT + num
                    else if (cfm) begin
                        state <= CHARGE;
                        // values
                        // coins1 <= coins1; coins0 <= coins0;
                    end // INPUT + cfm
                end
                CHARGE: begin
                    if (charge_minus && mins1 == 4'h0 && mins0 == 4'h1) begin
                        // next state
                        state <= START;
                        // values
                        coins1 <= 4'h0; coins0 <= 4'h0;
                        mins1 <= 4'h0; mins0 <= 4'h0;
                    end 
                    else if (charge_minus) begin
                        // next state
                        // state <= CHARGE
                        // values, coins keep, minutes minus
                        // coins1 <= coins1; coins0 <= coins0;
                        if (mins0 > 4'h0) begin
                            mins1 <= mins1; mins0 <= mins0 - 4'h1;
                        end
                        else begin
                            mins1 <= mins1 - 4'h1; mins0 <= 4'h9;
                        end
                    end
                end
            endcase
        end
    end
    
    // timer for no action
    always @ (posedge clk_1M or posedge rst) begin
        if (rst) begin
            timer_action <= 24'b0;
            no_action <= 1'b0;
        end
        else begin
            if (str | clr | charge_end) begin
                timer_action <= 24'b0;
                no_action <= 1'b0;
            end
            else if (en_timer_action) begin
                if (timer_action == NO_ACTION) begin
                    timer_action <= 24'b0;
                    no_action <= 1'b1;
                end
                else begin
                    timer_action <= timer_action + 24'b1;
                    no_action <= 1'b0;
                end
            end
        end
    end
    
    // timer for charge
    always @ (posedge clk_1M or posedge rst) begin
        if (rst) begin
            timer_charge <= 24'b0;
            charge_minus <= 1'b0;
            charge_end <= 1'b0;
        end
        else begin
            if (cfm) begin
                timer_charge <= 24'b0;
                charge_minus <= 1'b0;
                charge_end <= 1'b0;
            end
            else if (en_timer_charge) begin
                if (timer_charge == CHARGE_MINUS) begin
                    timer_charge <= 24'b0;
                    charge_minus <= 1'b1;
                    if (mins1 == 4'h0 && mins0 == 4'h1)
                        charge_end <= 1'b1;
                    else
                        charge_end <= 1'b0;
                end
                else begin
                    timer_charge <= timer_charge + 24'b1;
                    charge_minus <= 1'b0;
                    charge_end <= 1'b0;
                end
            end
        end
    end

    keyboard keyboard_inst(
        .clk(clk_1M),
        .rst(rst),
        .rows(rows),
        .cols(cols),
        .keys(keys),
        .valid(valid)
        );
    
    display display_inst(
        .clk(clk_1M),
        .rst(rst),
        .en(en_display),
        .bcd0(mins0),
        .bcd1(mins1),
        .bcd2(coins0),
        .bcd3(coins1),
        .segs(segs),
        .digs(digs)
        );
        
//    clk_divider #(10) clk_divider_inst(
//        .clk_in(clk_100M),
//        .rst(rst),
//        .clk_out(clk_1M)
//        );
       
endmodule