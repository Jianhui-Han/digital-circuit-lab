`timescale 1ns / 1ps

module keyboard(
    input clk,  // 1MHz
    input rst,
    input [3:0] rows,
    output [3:0] cols,
    output reg [3:0] keys,
    output valid
    );
    
    wire clk_1M; // input clock
    
    wire clk_1k; // clock for column scan
    reg col_scan_en;
    reg [1:0] cnt_col; 
    
    wire row_and; // reduced and of incoming rows
    wire row_and_neg; // negedge of row_and
    wire row_and_pos; // posedge of row_and
    reg row_and_d; // delay for row_and
    
    reg [3:0] cols_lock, rows_lock; // registers to lock cols and rows
    
    reg deb_en; // enable signal of debounce process
    reg [17:0] cnt_deb;
    reg row_and_deb, row_and_deb_d;
    
    integer MAX_DEB_CNT = 99999;
    
    // input clk
    assign clk_1M = clk;
    
    // column scan
    assign cols[0] = ~(~cnt_col[0] & ~cnt_col[1]);
    assign cols[1] = ~(cnt_col[0] & ~cnt_col[1]);
    assign cols[2] = ~(~cnt_col[0] & cnt_col[1]);
    assign cols[3] = ~(cnt_col[0] & cnt_col[1]);
    
    always @ (posedge clk_1k or posedge rst) begin
        if (rst)
            cnt_col <= 2'd0;
        else
            if (col_scan_en)
                if (cnt_col == 2'd3)
                    cnt_col <= 2'd0;
                else
                    cnt_col <= cnt_col + 1;
    end
    
    // detect the negedge of row_and
    assign row_and = &rows;
    assign row_and_neg = ~row_and & row_and_d;
    assign row_and_pos = row_and & ~row_and_d;
    
    always @ (posedge clk_1M or posedge rst) begin
        if (rst)
            row_and_d <= 1'd1;
        else
            row_and_d <= row_and;
    end
            
    
    // when negedge of row_and, lock cols and rows
    always @ (posedge clk_1M or posedge rst) begin
        if (rst) begin
            cols_lock <= 4'd0;
            rows_lock <= 4'd0;
        end
        else begin
            if (row_and_neg) begin
                cols_lock <= cols;
                rows_lock <= rows;
            end
        end
    end
    
    // column scan is disabled until the posedge of row_and
    always @ (posedge clk_1M or posedge rst) begin
        if (rst)
            col_scan_en <= 1'd1;
        else
            if (row_and_neg)
                col_scan_en <= 1'd0;
            else if (row_and_pos)
                col_scan_en <= 1'd1;
    end
    
    // deb_en is valid until the cnt_deb count to max once
    always @ (posedge clk_1M or posedge rst) begin
        if (rst)
            deb_en <= 1'd0;
        else
            if (row_and_neg)
                deb_en <= 1'd1;
            else if (cnt_deb == MAX_DEB_CNT)
                deb_en <= 1'd0;
    end
    
    // debounce
    assign valid = ~row_and_deb & row_and_deb_d;
    
    always @ (posedge clk_1M or posedge rst) begin
        if (rst)
            cnt_deb <= 15'd0;
        else
            if (row_and_neg)
                cnt_deb <= 15'd0;
            else if (cnt_deb == MAX_DEB_CNT)
                cnt_deb <= 15'd0;
            else if (deb_en)
                cnt_deb <= cnt_deb + 1;
    end
    
    always @ (posedge clk_1M or posedge rst) begin
        if (rst) begin
            row_and_deb <= 1'd1;
            row_and_deb_d <= 1'd1;
        end
        else begin
            if (deb_en & cnt_deb == MAX_DEB_CNT)
                row_and_deb <= row_and;
            else
                row_and_deb <= 1'd1;
            row_and_deb_d <= row_and_deb;
        end
    end
    
    // decode keys
    always @ (*) begin
        case({cols_lock, rows_lock})
            8'hee: keys = 4'h1;
            8'hde: keys = 4'h2;
            8'hbe: keys = 4'h3;
            8'h7e: keys = 4'h0;
            8'hed: keys = 4'h4;
            8'hdd: keys = 4'h5;
            8'hbd: keys = 4'h6;
            8'h7d: keys = 4'h0;
            8'heb: keys = 4'h7;
            8'hdb: keys = 4'h8;
            8'hbb: keys = 4'h9;
            8'h7b: keys = 4'h0;
            8'he7: keys = 4'h0;
            8'hd7: keys = 4'ha;
            8'hb7: keys = 4'hb;
            8'h77: keys = 4'hc;
            default: keys = 4'h0;
        endcase
    end

    // clk dividers
    clk_divider #(1000) clk_divider_inst(
        .clk_in(clk_1M),
        .rst(rst),
        .clk_out(clk_1k)
        );

endmodule