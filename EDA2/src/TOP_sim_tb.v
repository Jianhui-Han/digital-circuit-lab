`timescale 100ps / 1ps

// here are 11 cases to be tested:
// 0. INIT + start
// 1. INIT + other input (illegal)
// 2. START + start/clear/confirm (illegal)
// 3. START + 0 (illegal)
// 4. START + input
// 5. START + no action 
// 6. INPUT + input
// 7. INPUT + concurrent input
// 8. INPUT + start (illegal)
// 9. INPUT + clear
// 10. INPUT + confirm
// 11. CHARGE + any input (illegal)

module TOP_sim_tb();

    reg clk, rst;
    reg [3:0] rows;
    
    wire [3:0] cols, digs;
    wire [6:0] segs;
    
    reg [3:0] key_sim_rows;
    wire [3:0] key_sim_cols;
    wire [3:0] key_sim_keys;
    wire key_sim_valid;
    
    parameter base1 = 175095;
    parameter one_ms = 10000;
    parameter two_ms = 20000;
    parameter three_ms = 30000;
    parameter fifty_ms = 500000;
    parameter two_hun_ms = 2000000;
    parameter one_thu_ms = 10000000;
    parameter base2 = 118970280;
    
    initial clk = 0;
    always #5 clk = ~clk;
    
    initial begin
        rst = 1; #100 rst = 0;
    end
    
    initial begin
        rows = 4'b1111;
        #base1 rows = 4'b1011;                                          // case 1
        #two_hun_ms rows = 4'b1111;
        #two_hun_ms rows = 4'b0111;                                     // case 0
        #two_hun_ms rows = 4'b1111;
        #two_hun_ms rows = 4'b0111;                                     // case 2.0
        #two_hun_ms rows = 4'b1111;
        #two_hun_ms #one_ms rows = 4'b0111;                             // case 2.1
        #two_hun_ms rows = 4'b1111;
        #two_hun_ms #one_ms rows = 4'b0111;                             // case 2.2
        #two_hun_ms rows = 4'b1111;
        #two_hun_ms #one_ms rows = 4'b0111;                             // case 3
        #two_hun_ms rows = 4'b1111;
        #two_hun_ms rows = 4'b1110;                                     // case 4: input 1
        #two_hun_ms rows = 4'b1111;
        #two_hun_ms #two_ms rows = 4'b1110;                             // case 6: input 3
        #two_hun_ms rows = 4'b1111;
        #two_hun_ms #one_ms rows = 4'b0111;                             // case 10
        #two_hun_ms rows = 4'b1111;
        #two_hun_ms rows = 4'b0111;                                     // case 11.0: input confirm
        #two_hun_ms rows = 4'b1111;
        #two_hun_ms #one_ms rows = 4'b1011;                             // case 11.1: input number 7
        #two_hun_ms rows = 4'b1111;
        #two_hun_ms #one_ms rows = 4'b0111;                             // case 11.2: input start
        #two_hun_ms rows = 4'b1111;
        #two_hun_ms #one_ms rows = 4'b0111;                             // case 11.3: input clear 
        #two_hun_ms rows = 4'b1111;
        #base2 #one_ms rows = 4'b1110;                                  // input 2
        #two_hun_ms rows = 4'b1111;
        #two_hun_ms rows = 4'b0111;                                     // case 8
        #two_hun_ms rows = 4'b1111;
        #two_hun_ms #one_ms rows = 4'b0111;                             // case 9
        #two_hun_ms rows = 4'b1111;
        #two_hun_ms #two_ms rows = 4'b1110;                             // case 7.0: input 1
        #two_hun_ms rows = 4'b1111;
        #two_hun_ms #two_ms rows = 4'b1101;                             // case 7.1: input 6
        #two_hun_ms rows = 4'b1111;
        #two_hun_ms #three_ms rows = 4'b1011;                           // case 7.3: input 5
        #two_hun_ms rows = 4'b1111;
        #two_hun_ms #three_ms rows = 4'b1101;                           // case 7.4: input 4
        #two_hun_ms rows = 4'b1111;
        #two_hun_ms #three_ms rows = 4'b0111;                           // input confirm
        #two_hun_ms rows = 4'b1111;                                     // case 5: just wait
    end
    
    initial begin
        key_sim_rows = 4'b1111;
        #base1 key_sim_rows = 4'b1110;
        #two_hun_ms key_sim_rows = 4'b1111;
        #two_hun_ms key_sim_rows = 4'b1101;
        #two_hun_ms key_sim_rows = 4'b1111;
        #two_hun_ms key_sim_rows = 4'b1011;
        #two_hun_ms key_sim_rows = 4'b1111;
        #two_hun_ms key_sim_rows = 4'b0111;
        #two_hun_ms key_sim_rows = 4'b1111;
        #two_hun_ms #one_ms key_sim_rows = 4'b1110;
        #two_hun_ms key_sim_rows = 4'b1111;
        #two_hun_ms key_sim_rows = 4'b1101;
        #two_hun_ms key_sim_rows = 4'b1111;
        #two_hun_ms key_sim_rows = 4'b1011;
        #two_hun_ms key_sim_rows = 4'b1111;
        #two_hun_ms key_sim_rows = 4'b0111;
        #two_hun_ms key_sim_rows = 4'b1111;
        #two_hun_ms #one_ms key_sim_rows = 4'b1110;
        #two_hun_ms key_sim_rows = 4'b1111;
        #two_hun_ms key_sim_rows = 4'b1101;
        #two_hun_ms key_sim_rows = 4'b1111;
        #two_hun_ms key_sim_rows = 4'b1011;
        #two_hun_ms key_sim_rows = 4'b1111;
        #two_hun_ms key_sim_rows = 4'b0111;
        #two_hun_ms key_sim_rows = 4'b1111;
        #two_hun_ms #one_ms key_sim_rows = 4'b1110;
        #two_hun_ms key_sim_rows = 4'b1111;
        #two_hun_ms key_sim_rows = 4'b1101;
        #two_hun_ms key_sim_rows = 4'b1111;
        #two_hun_ms key_sim_rows = 4'b1011;
        #two_hun_ms key_sim_rows = 4'b1111;
        #two_hun_ms key_sim_rows = 4'b0111;
        #two_hun_ms key_sim_rows = 4'b1111;
        #two_hun_ms #two_hun_ms key_sim_rows = 4'b0111;
        #fifty_ms key_sim_rows = 4'b1111;
        #two_hun_ms key_sim_rows = 4'b1011;
        #one_thu_ms key_sim_rows = 4'b1111;                 
    end
    
    TOP_sim TOP_sim_inst(
        .clk(clk),
        .rst(rst),
        .rows(rows),
        .cols(cols),
        .segs(segs),
        .digs(digs)
    );
    
    keyboard key_board_sim_inst(
        .clk(clk),
        .rst(rst),
        .rows(key_sim_rows),
        .cols(key_sim_cols),
        .keys(key_sim_keys),
        .valid(key_sim_valid)
        );

endmodule
