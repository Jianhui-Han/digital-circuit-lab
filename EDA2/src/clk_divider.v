`timescale 1ns / 1ps

module clk_divider #(parameter N = 100)(
    input clk_in,
    input rst,
    output reg clk_out
    );
    
    reg [31:0] cnt;
    integer MAX = N / 2 - 1;
    
    always @ (posedge clk_in or posedge rst) begin
        if (rst) begin
            cnt <= 32'd0;
            clk_out <= 1'b0;
        end
        else begin
            if (cnt == MAX) begin
                cnt <= 32'd0;
                clk_out <= ~clk_out;
            end
            else
                cnt <= cnt + 1;
        end
    end
           
endmodule
