`timescale 1ns / 1ps

module display(
    input clk,  // 1MHz
    input rst,
    input en,
    input [3:0] bcd0,
    input [3:0] bcd1,
    input [3:0] bcd2,
    input [3:0] bcd3,
    output [6:0] segs,
    output [3:0] digs
    );
    
    wire clk_1M, clk_250;
    wire [3:0] bcd, bcd_c, bcd_m;
    
    reg [1:0] cnt;
    reg [3:0] digs_reg; 
    
    assign clk_1M = clk;
    assign bcd_c = cnt[0] ? bcd3 : bcd2;
    assign bcd_m = cnt[0] ? bcd1 : bcd0;
    assign bcd = cnt[1] ? bcd_c : bcd_m;
    assign digs = en ? digs_reg : 4'b1111;
    
    always @ (*) begin
        case(cnt)
            2'd0: digs_reg = 4'b1110;
            2'd1: digs_reg = 4'b1101;
            2'd2: digs_reg = 4'b1011;
            2'd3: digs_reg = 4'b0111;
        endcase
    end
    
    always @ (posedge clk_250 or posedge rst) begin
        if (rst)
            cnt <= 2'd0;
        else
            if (cnt == 2'd3)
                cnt <= 2'd0;
            else
                cnt <= cnt + 2'd1;
    end
    
    clk_divider #(4000) clk_divider_inst(
        .clk_in(clk_1M),
        .rst(rst),
        .clk_out(clk_250)
        );
    
    decoder decoder_inst(
        .bcd(bcd),
        .seg(segs)
        );
        
        
endmodule