`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Module name: bcd_decade_counter
// Description: A 4-bit synchronous bcd decade counter
// Parameters: DELAY
//////////////////////////////////////////////////////////////////////////////////


module bcd_decade_counter #(parameter DELAY = 10)(
    input CEP, CET,
    input PE_n,
    input P0, P1, P2, P3,
    input MR_n,
    input CP,
    output Q0, Q1, Q2, Q3,
    output TC
    );
    
    reg [3:0] cnt;
    
    always @ (posedge CP or negedge MR_n) begin
        if (!MR_n) begin
            cnt <= 4'd0;
        end
        else begin
            if (!PE_n)
                cnt <= {P3, P2, P1, P0};
            else if (PE_n && CEP && CET)
                if (cnt == 4'd11 || cnt == 4'd13)
                    cnt <= 4'd4;
                else if (cnt == 4'd9 || cnt == 4'd15)
                    cnt <= 4'd0;
                else
                    cnt <= cnt + 1'b1;
        end
    end
    
    assign #DELAY Q3 = cnt[3];
    assign #DELAY Q2 = cnt[2];
    assign #DELAY Q1 = cnt[1];
    assign #DELAY Q0 = cnt[0];
    assign #DELAY TC = CET & cnt[0] & ~cnt[1] & ~cnt[2] & cnt[3]; 
    
endmodule
