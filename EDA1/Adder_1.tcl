set project_name Adder_1
file mkdir $project_name
cd ./$project_name
create_project -force $project_name ./ -part xc7a35tcpg236-1
set_property target_language verilog [current_project]
set_property simulator_language verilog [current_project]
set_property ip_repo_paths ../../LIBS [current_project]
update_ip_catalog

create_bd_design "$project_name"

set xup_and2_0 [ create_bd_cell -type ip -vlnv xilinx.com:xup:xup_and2:1.0 xup_and2_0 ]
set xup_and2_1 [ create_bd_cell -type ip -vlnv xilinx.com:xup:xup_and2:1.0 xup_and2_1 ]
set xup_or2_0 [ create_bd_cell -type ip -vlnv xilinx.com:xup:xup_or2:1.0 xup_or2_0 ]
set xup_xor2_0 [ create_bd_cell -type ip -vlnv xilinx.com:xup:xup_xor2:1.0 xup_xor2_0 ]
set xup_xor2_1 [ create_bd_cell -type ip -vlnv xilinx.com:xup:xup_xor2:1.0 xup_xor2_1 ]

set A [ create_bd_port -dir I A ]
set B [ create_bd_port -dir I B ]
set CI [ create_bd_port -dir I CI ]
set CO [ create_bd_port -dir O CO ]
set S [ create_bd_port -dir O S ]

connect_bd_net -net A_1 [get_bd_ports A] [get_bd_pins xup_and2_0/a] [get_bd_pins xup_xor2_0/a]
connect_bd_net -net B_1 [get_bd_ports B] [get_bd_pins xup_and2_0/b] [get_bd_pins xup_xor2_0/b]
connect_bd_net -net CI_1 [get_bd_ports CI] [get_bd_pins xup_and2_1/b] [get_bd_pins xup_xor2_1/b]
connect_bd_net -net xup_and2_0_y [get_bd_pins xup_and2_0/y] [get_bd_pins xup_or2_0/a]
connect_bd_net -net xup_and2_1_y [get_bd_pins xup_and2_1/y] [get_bd_pins xup_or2_0/b]
connect_bd_net -net xup_or2_0_y [get_bd_ports CO] [get_bd_pins xup_or2_0/y]
connect_bd_net -net xup_xor2_0_y [get_bd_pins xup_and2_1/a] [get_bd_pins xup_xor2_0/y] [get_bd_pins xup_xor2_1/a]
connect_bd_net -net xup_xor2_1_y [get_bd_ports S] [get_bd_pins xup_xor2_1/y]

regenerate_bd_layout
save_bd_design
generate_target all [get_files ./$project_name.srcs/sources_1/bd/$project_name/$project_name.bd]

make_wrapper -files [get_files ./$project_name.srcs/sources_1/bd/$project_name/$project_name.bd] -top
add_files -norecurse ./$project_name.srcs/sources_1/bd/$project_name/hdl/$project_name\_wrapper.v

add_files -fileset sim_1 -norecurse -copy_to ./$project_name.srcs/sim_1/new ../src/$project_name\_TB.v

update_compile_order -fileset sources_1
update_compile_order -fileset sim_1

launch_runs synth_1 -jobs 2
wait_on_run synth_1

ipx::package_project -root_dir ../../LIBS/MY_LIB/$project_name -vendor xilinx.com -library user -taxonomy /UserIP -module $project_name -import_files -force

close_project
cd ../
