set project_name Operator
file mkdir $project_name
cd ./$project_name
create_project -force $project_name ./ -part xc7a35tcpg236-1
set_property target_language verilog [current_project]
set_property simulator_language verilog [current_project]
set_property ip_repo_paths ../../LIBS [current_project]
update_ip_catalog

create_bd_design "$project_name"

set Complement_0 [ create_bd_cell -type ip -vlnv xilinx.com:user:Complement:1.0 Complement_0 ]
set Complement_1 [ create_bd_cell -type ip -vlnv xilinx.com:user:Complement:1.0 Complement_1 ]
set Complement_2 [ create_bd_cell -type ip -vlnv xilinx.com:user:Complement:1.0 Complement_2 ]
set Adder_4_0 [ create_bd_cell -type ip -vlnv xilinx.com:user:Adder_4:1.0 Adder_4_0 ]
set xlconstant_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_0 ]
set_property -dict [ list CONFIG.CONST_VAL {0} ] $xlconstant_0
set xup_2_to_1_mux_0 [ create_bd_cell -type ip -vlnv xilinx.com:xup:xup_2_to_1_mux:1.0 xup_2_to_1_mux_0 ]
set xup_and2_0 [ create_bd_cell -type ip -vlnv xilinx.com:xup:xup_and2:1.0 xup_and2_0 ]
set xup_and2_1 [ create_bd_cell -type ip -vlnv xilinx.com:xup:xup_and2:1.0 xup_and2_1 ]
set xup_or3_0 [ create_bd_cell -type ip -vlnv xilinx.com:xup:xup_or3:1.0 xup_or3_0 ]
set xup_or3_1 [ create_bd_cell -type ip -vlnv xilinx.com:xup:xup_or3:1.0 xup_or3_1 ]
set xup_xor2_0 [ create_bd_cell -type ip -vlnv xilinx.com:xup:xup_xor2:1.0 xup_xor2_0 ]

set M0 [ create_bd_port -dir I M0 ]
set M1 [ create_bd_port -dir I M1 ]
set M2 [ create_bd_port -dir I M2 ]
set M3 [ create_bd_port -dir I M3 ]
set N0 [ create_bd_port -dir I N0 ]
set N1 [ create_bd_port -dir I N1 ]
set N2 [ create_bd_port -dir I N2 ]
set N3 [ create_bd_port -dir I N3 ]
set S0 [ create_bd_port -dir O S0 ]
set S1 [ create_bd_port -dir O S1 ]
set S2 [ create_bd_port -dir O S2 ]
set S3 [ create_bd_port -dir O S3 ]
set SIG [ create_bd_port -dir O SIG ]
set SIG_IN_M [ create_bd_port -dir O SIG_IN_M ]
set SIG_IN_N [ create_bd_port -dir O SIG_IN_N ]

connect_bd_net -net Complement_0_AC0 [get_bd_pins Complement_0/AC0] [get_bd_pins Adder_4_0/A0]
connect_bd_net -net Complement_0_AC1 [get_bd_pins Complement_0/AC1] [get_bd_pins Adder_4_0/A1]
connect_bd_net -net Complement_0_AC2 [get_bd_pins Complement_0/AC2] [get_bd_pins Adder_4_0/A2]
connect_bd_net -net Complement_1_AC0 [get_bd_pins Complement_1/AC0] [get_bd_pins Adder_4_0/B0]
connect_bd_net -net Complement_1_AC1 [get_bd_pins Complement_1/AC1] [get_bd_pins Adder_4_0/B1]
connect_bd_net -net Complement_1_AC2 [get_bd_pins Complement_1/AC2] [get_bd_pins Adder_4_0/B2]
connect_bd_net -net Complement_2_AC0 [get_bd_ports S0] [get_bd_pins Complement_2/AC0]
connect_bd_net -net Complement_2_AC1 [get_bd_ports S1] [get_bd_pins Complement_2/AC1]
connect_bd_net -net Complement_2_AC2 [get_bd_ports S2] [get_bd_pins Complement_2/AC2]
connect_bd_net -net Complement_2_AC3 [get_bd_ports S3] [get_bd_pins Complement_2/AC3]
connect_bd_net -net Adder_4_0_CO [get_bd_pins Adder_4_0/CO] [get_bd_pins xup_2_to_1_mux_0/a]
connect_bd_net -net Adder_4_0_S0 [get_bd_pins Complement_2/A0] [get_bd_pins Adder_4_0/S0]
connect_bd_net -net Adder_4_0_S1 [get_bd_pins Complement_2/A1] [get_bd_pins Adder_4_0/S1]
connect_bd_net -net Adder_4_0_S2 [get_bd_pins Complement_2/A2] [get_bd_pins Adder_4_0/S2]
connect_bd_net -net Adder_4_0_S3 [get_bd_pins Complement_2/A3] [get_bd_pins Adder_4_0/S3] [get_bd_pins xup_2_to_1_mux_0/b]
connect_bd_net -net M0_1 [get_bd_ports M0] [get_bd_pins Complement_0/A0] [get_bd_pins xup_or3_0/a]
connect_bd_net -net M1_1 [get_bd_ports M1] [get_bd_pins Complement_0/A1] [get_bd_pins xup_or3_0/b]
connect_bd_net -net M2_1 [get_bd_ports M2] [get_bd_pins Complement_0/A2] [get_bd_pins xup_or3_0/c]
connect_bd_net -net M3_2 [get_bd_ports M3] [get_bd_pins xup_and2_0/b]
connect_bd_net -net N0_1 [get_bd_ports N0] [get_bd_pins Complement_1/A0] [get_bd_pins xup_or3_1/a]
connect_bd_net -net N1_1 [get_bd_ports N1] [get_bd_pins Complement_1/A1] [get_bd_pins xup_or3_1/b]
connect_bd_net -net N2_1 [get_bd_ports N2] [get_bd_pins Complement_1/A2] [get_bd_pins xup_or3_1/c]
connect_bd_net -net N3_2 [get_bd_ports N3] [get_bd_pins xup_and2_1/b]
connect_bd_net -net xlconstant_0_dout [get_bd_pins Complement_0/A3] [get_bd_pins Complement_1/A3] [get_bd_pins Adder_4_0/CI] [get_bd_pins xlconstant_0/dout]
connect_bd_net -net xup_2_to_1_mux_0_y [get_bd_ports SIG] [get_bd_pins Complement_2/A4] [get_bd_pins xup_2_to_1_mux_0/y]
connect_bd_net -net xup_and2_0_y [get_bd_ports SIG_IN_M] [get_bd_pins Complement_0/A4] [get_bd_pins Adder_4_0/A3] [get_bd_pins xup_and2_0/y] [get_bd_pins xup_xor2_0/a]
connect_bd_net -net xup_and2_1_y [get_bd_ports SIG_IN_N] [get_bd_pins Complement_1/A4] [get_bd_pins Adder_4_0/B3] [get_bd_pins xup_and2_1/y] [get_bd_pins xup_xor2_0/b]
connect_bd_net -net xup_or3_0_y [get_bd_pins xup_and2_0/a] [get_bd_pins xup_or3_0/y]
connect_bd_net -net xup_or3_1_y [get_bd_pins xup_and2_1/a] [get_bd_pins xup_or3_1/y]
connect_bd_net -net xup_xor2_0_y [get_bd_pins xup_2_to_1_mux_0/select] [get_bd_pins xup_xor2_0/y]

regenerate_bd_layout
save_bd_design
generate_target all [get_files ./$project_name.srcs/sources_1/bd/$project_name/$project_name.bd]

make_wrapper -files [get_files ./$project_name.srcs/sources_1/bd/$project_name/$project_name.bd] -top
add_files -norecurse ./$project_name.srcs/sources_1/bd/$project_name/hdl/$project_name\_wrapper.v

add_files -fileset sim_1 -norecurse -copy_to ./$project_name.srcs/sim_1/new ../src/$project_name\_TB.v

update_compile_order -fileset sources_1
update_compile_order -fileset sim_1

launch_runs synth_1 -jobs 2
wait_on_run synth_1

ipx::package_project -root_dir ../../LIBS/MY_LIB/$project_name -vendor xilinx.com -library user -taxonomy /UserIP -module $project_name -import_files -force

close_project
cd ../
