set project_name Adder_4
file mkdir $project_name
cd ./$project_name
create_project -force $project_name ./ -part xc7a35tcpg236-1
set_property target_language verilog [current_project]
set_property simulator_language verilog [current_project]
set_property ip_repo_paths ../../LIBS [current_project]
update_ip_catalog

create_bd_design "$project_name"

set Adder_1_0 [ create_bd_cell -type ip -vlnv xilinx.com:user:Adder_1:1.0 Adder_1_0 ]
set Adder_1_1 [ create_bd_cell -type ip -vlnv xilinx.com:user:Adder_1:1.0 Adder_1_1 ]
set Adder_1_2 [ create_bd_cell -type ip -vlnv xilinx.com:user:Adder_1:1.0 Adder_1_2 ]
set Adder_1_3 [ create_bd_cell -type ip -vlnv xilinx.com:user:Adder_1:1.0 Adder_1_3 ]

set A0 [ create_bd_port -dir I A0 ]
set A1 [ create_bd_port -dir I A1 ]
set A2 [ create_bd_port -dir I A2 ]
set A3 [ create_bd_port -dir I A3 ]
set B0 [ create_bd_port -dir I B0 ]
set B1 [ create_bd_port -dir I B1 ]
set B2 [ create_bd_port -dir I B2 ]
set B3 [ create_bd_port -dir I B3 ]
set CI [ create_bd_port -dir I CI ]
set CO [ create_bd_port -dir O CO ]
set S0 [ create_bd_port -dir O S0 ]
set S1 [ create_bd_port -dir O S1 ]
set S2 [ create_bd_port -dir O S2 ]
set S3 [ create_bd_port -dir O S3 ]

connect_bd_net -net A0_1 [get_bd_ports A0] [get_bd_pins Adder_1_0/A]
connect_bd_net -net A1_1 [get_bd_ports A1] [get_bd_pins Adder_1_1/A]
connect_bd_net -net A2_1 [get_bd_ports A2] [get_bd_pins Adder_1_2/A]
connect_bd_net -net A3_1 [get_bd_ports A3] [get_bd_pins Adder_1_3/A]
connect_bd_net -net Adder_1_0_CO [get_bd_pins Adder_1_0/CO] [get_bd_pins Adder_1_1/CI]
connect_bd_net -net Adder_1_0_S [get_bd_ports S0] [get_bd_pins Adder_1_0/S]
connect_bd_net -net Adder_1_1_CO [get_bd_pins Adder_1_1/CO] [get_bd_pins Adder_1_2/CI]
connect_bd_net -net Adder_1_1_S [get_bd_ports S1] [get_bd_pins Adder_1_1/S]
connect_bd_net -net Adder_1_2_CO [get_bd_pins Adder_1_2/CO] [get_bd_pins Adder_1_3/CI]
connect_bd_net -net Adder_1_2_S [get_bd_ports S2] [get_bd_pins Adder_1_2/S]
connect_bd_net -net Adder_1_3_CO [get_bd_ports CO] [get_bd_pins Adder_1_3/CO]
connect_bd_net -net Adder_1_3_S [get_bd_ports S3] [get_bd_pins Adder_1_3/S]
connect_bd_net -net B0_1 [get_bd_ports B0] [get_bd_pins Adder_1_0/B]
connect_bd_net -net B1_1 [get_bd_ports B1] [get_bd_pins Adder_1_1/B]
connect_bd_net -net B2_1 [get_bd_ports B2] [get_bd_pins Adder_1_2/B]
connect_bd_net -net B3_1 [get_bd_ports B3] [get_bd_pins Adder_1_3/B]
connect_bd_net -net CI_1 [get_bd_ports CI] [get_bd_pins Adder_1_0/CI]

regenerate_bd_layout
save_bd_design
generate_target all [get_files ./$project_name.srcs/sources_1/bd/$project_name/$project_name.bd]

make_wrapper -files [get_files ./$project_name.srcs/sources_1/bd/$project_name/$project_name.bd] -top
add_files -norecurse ./$project_name.srcs/sources_1/bd/$project_name/hdl/$project_name\_wrapper.v

add_files -fileset sim_1 -norecurse -copy_to ./$project_name.srcs/sim_1/new ../src/$project_name\_TB.v

update_compile_order -fileset sources_1
update_compile_order -fileset sim_1

launch_runs synth_1 -jobs 2
wait_on_run synth_1

ipx::package_project -root_dir ../../LIBS/MY_LIB/$project_name -vendor xilinx.com -library user -taxonomy /UserIP -module $project_name -import_files -force

close_project
cd ../
