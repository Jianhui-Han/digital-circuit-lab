set project_name Display_sw
file mkdir $project_name
cd ./$project_name
create_project -force $project_name ./ -part xc7a35tcpg236-1
set_property target_language verilog [current_project]
set_property simulator_language verilog [current_project]
set_property ip_repo_paths ../../LIBS [current_project]
update_ip_catalog

create_bd_design "$project_name"

set Decoder_47_0 [ create_bd_cell -type ip -vlnv xilinx.com:user:Decoder_47:1.0 Decoder_47_0 ]
set bin2BCD_0 [ create_bd_cell -type ip -vlnv xilinx.com:XUP:bin2BCD:1.0 bin2BCD_0 ]
set xlconcat_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 xlconcat_0 ]
set_property -dict [ list CONFIG.NUM_PORTS {4} ] $xlconcat_0
set xlconcat_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 xlconcat_1 ]
set_property -dict [ list CONFIG.NUM_PORTS {4} ] $xlconcat_1
set xlconcat_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 xlconcat_2 ]
set_property -dict [ list CONFIG.NUM_PORTS {4} ] $xlconcat_2
set xlconcat_3 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 xlconcat_3 ]
set_property -dict [ list CONFIG.NUM_PORTS {4} ] $xlconcat_3
set xlconcat_4 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 xlconcat_4 ]
set xlconstant_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_0 ]
set_property -dict [ list CONFIG.CONST_VAL {0} ] $xlconstant_0
set xlslice_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_0 ]
set_property -dict [ list CONFIG.DIN_FROM {0} CONFIG.DIN_TO {0} CONFIG.DIN_WIDTH {4} ] $xlslice_0
set xlslice_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_1 ]
set_property -dict [ list CONFIG.DIN_FROM {1} CONFIG.DIN_TO {1} CONFIG.DIN_WIDTH {4} CONFIG.DOUT_WIDTH {1} ] $xlslice_1
set xlslice_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_2 ]
set_property -dict [ list CONFIG.DIN_FROM {2} CONFIG.DIN_TO {2} CONFIG.DIN_WIDTH {4} CONFIG.DOUT_WIDTH {1} ] $xlslice_2
set xlslice_3 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_3 ]
set_property -dict [ list CONFIG.DIN_FROM {3} CONFIG.DIN_TO {3} CONFIG.DIN_WIDTH {4} CONFIG.DOUT_WIDTH {1} ] $xlslice_3
set xup_4_to_1_mux_vector_0 [ create_bd_cell -type ip -vlnv xilinx.com:xup:xup_4_to_1_mux_vector:1.0 xup_4_to_1_mux_vector_0 ]
set xup_inv_0 [ create_bd_cell -type ip -vlnv xilinx.com:xup:xup_inv:1.0 xup_inv_0 ]
set xup_inv_1 [ create_bd_cell -type ip -vlnv xilinx.com:xup:xup_inv:1.0 xup_inv_1 ]
set xup_nand2_0 [ create_bd_cell -type ip -vlnv xilinx.com:xup:xup_nand2:1.0 xup_nand2_0 ]
set xup_nand2_1 [ create_bd_cell -type ip -vlnv xilinx.com:xup:xup_nand2:1.0 xup_nand2_1 ]
set xup_nand2_2 [ create_bd_cell -type ip -vlnv xilinx.com:xup:xup_nand2:1.0 xup_nand2_2 ]
set xup_nand2_3 [ create_bd_cell -type ip -vlnv xilinx.com:xup:xup_nand2:1.0 xup_nand2_3 ]

set A0 [ create_bd_port -dir I A0 ]
set A1 [ create_bd_port -dir I A1 ]
set A2 [ create_bd_port -dir I A2 ]
set AN0 [ create_bd_port -dir O AN0 ]
set AN1 [ create_bd_port -dir O AN1 ]
set AN2 [ create_bd_port -dir O AN2 ]
set AN3 [ create_bd_port -dir O AN3 ]
set B0 [ create_bd_port -dir I B0 ]
set B1 [ create_bd_port -dir I B1 ]
set B2 [ create_bd_port -dir I B2 ]
set L0 [ create_bd_port -dir O L0 ]
set L1 [ create_bd_port -dir O L1 ]
set L2 [ create_bd_port -dir O L2 ]
set S0 [ create_bd_port -dir I S0 ]
set S1 [ create_bd_port -dir I S1 ]
set S2 [ create_bd_port -dir I S2 ]
set S3 [ create_bd_port -dir I S3 ]
set SIG [ create_bd_port -dir I SIG ]
set SIG_IN_M [ create_bd_port -dir I SIG_IN_M ]
set SIG_IN_N [ create_bd_port -dir I SIG_IN_N ]
set SW0 [ create_bd_port -dir I SW0 ]
set SW1 [ create_bd_port -dir I SW1 ]
set a [ create_bd_port -dir O a ]
set b [ create_bd_port -dir O b ]
set c [ create_bd_port -dir O c ]
set d [ create_bd_port -dir O d ]
set e [ create_bd_port -dir O e ]
set f [ create_bd_port -dir O f ]
set g [ create_bd_port -dir O g ]

connect_bd_net -net A0_1 [get_bd_ports A0] [get_bd_pins xlconcat_0/In0]
connect_bd_net -net A1_1 [get_bd_ports A1] [get_bd_pins xlconcat_0/In1]
connect_bd_net -net A2_1 [get_bd_ports A2] [get_bd_pins xlconcat_0/In2]
connect_bd_net -net A3_1 [get_bd_ports L0] [get_bd_ports SIG_IN_M]
connect_bd_net -net B0_1 [get_bd_ports B0] [get_bd_pins xlconcat_1/In0]
connect_bd_net -net B1_1 [get_bd_ports B1] [get_bd_pins xlconcat_1/In1]
connect_bd_net -net B2_1 [get_bd_ports B2] [get_bd_pins xlconcat_1/In2]
connect_bd_net -net B3_1 [get_bd_ports L1] [get_bd_ports SIG_IN_N]
connect_bd_net -net Decoder_47_0_a [get_bd_ports a] [get_bd_pins Decoder_47_0/a]
connect_bd_net -net Decoder_47_0_b [get_bd_ports b] [get_bd_pins Decoder_47_0/b]
connect_bd_net -net Decoder_47_0_c [get_bd_ports c] [get_bd_pins Decoder_47_0/c]
connect_bd_net -net Decoder_47_0_d [get_bd_ports d] [get_bd_pins Decoder_47_0/d]
connect_bd_net -net Decoder_47_0_e [get_bd_ports e] [get_bd_pins Decoder_47_0/e]
connect_bd_net -net Decoder_47_0_f [get_bd_ports f] [get_bd_pins Decoder_47_0/f]
connect_bd_net -net Decoder_47_0_g [get_bd_ports g] [get_bd_pins Decoder_47_0/g]
connect_bd_net -net S0_1 [get_bd_ports S0] [get_bd_pins xlconcat_3/In0]
connect_bd_net -net S1_1 [get_bd_ports S1] [get_bd_pins bin2BCD_0/a]
connect_bd_net -net S2_1 [get_bd_ports S2] [get_bd_pins bin2BCD_0/b]
connect_bd_net -net S3_1 [get_bd_ports S3] [get_bd_pins bin2BCD_0/c]
connect_bd_net -net SIG_1 [get_bd_ports L2] [get_bd_ports SIG]
connect_bd_net -net SW0_1 [get_bd_ports SW0] [get_bd_pins xlconcat_4/In0] [get_bd_pins xup_inv_0/a] [get_bd_pins xup_nand2_0/a] [get_bd_pins xup_nand2_2/a]
connect_bd_net -net SW1_1 [get_bd_ports SW1] [get_bd_pins xlconcat_4/In1] [get_bd_pins xup_inv_1/a] [get_bd_pins xup_nand2_0/b] [get_bd_pins xup_nand2_1/b]
connect_bd_net -net bin2BCD_0_y1 [get_bd_pins bin2BCD_0/y1] [get_bd_pins xlconcat_3/In1]
connect_bd_net -net bin2BCD_0_y2 [get_bd_pins bin2BCD_0/y2] [get_bd_pins xlconcat_3/In2]
connect_bd_net -net bin2BCD_0_y3 [get_bd_pins bin2BCD_0/y3] [get_bd_pins xlconcat_3/In3]
connect_bd_net -net bin2BCD_0_y4 [get_bd_pins bin2BCD_0/y4] [get_bd_pins xlconcat_2/In0]
connect_bd_net -net bin2BCD_0_y5 [get_bd_pins bin2BCD_0/y5] [get_bd_pins xlconcat_2/In1]
connect_bd_net -net bin2BCD_0_y6 [get_bd_pins bin2BCD_0/y6] [get_bd_pins xlconcat_2/In2]
connect_bd_net -net xlconcat_0_dout [get_bd_pins xlconcat_0/dout] [get_bd_pins xup_4_to_1_mux_vector_0/a]
connect_bd_net -net xlconcat_1_dout [get_bd_pins xlconcat_1/dout] [get_bd_pins xup_4_to_1_mux_vector_0/b]
connect_bd_net -net xlconcat_2_dout [get_bd_pins xlconcat_2/dout] [get_bd_pins xup_4_to_1_mux_vector_0/c]
connect_bd_net -net xlconcat_3_dout [get_bd_pins xlconcat_3/dout] [get_bd_pins xup_4_to_1_mux_vector_0/d]
connect_bd_net -net xlconcat_4_dout [get_bd_pins xlconcat_4/dout] [get_bd_pins xup_4_to_1_mux_vector_0/sel]
connect_bd_net -net xlconstant_0_dout [get_bd_pins bin2BCD_0/d] [get_bd_pins bin2BCD_0/e] [get_bd_pins bin2BCD_0/g] [get_bd_pins xlconcat_0/In3] [get_bd_pins xlconcat_1/In3] [get_bd_pins xlconcat_2/In3] [get_bd_pins xlconstant_0/dout]
connect_bd_net -net xlslice_0_Dout [get_bd_pins Decoder_47_0/BCD_A] [get_bd_pins xlslice_0/Dout]
connect_bd_net -net xlslice_1_Dout [get_bd_pins Decoder_47_0/BCD_B] [get_bd_pins xlslice_1/Dout]
connect_bd_net -net xlslice_2_Dout [get_bd_pins Decoder_47_0/BCD_C] [get_bd_pins xlslice_2/Dout]
connect_bd_net -net xlslice_3_Dout [get_bd_pins Decoder_47_0/BCD_D] [get_bd_pins xlslice_3/Dout]
connect_bd_net -net xup_4_to_1_mux_vector_0_y [get_bd_pins xlslice_0/Din] [get_bd_pins xlslice_1/Din] [get_bd_pins xlslice_2/Din] [get_bd_pins xlslice_3/Din] [get_bd_pins xup_4_to_1_mux_vector_0/y]
connect_bd_net -net xup_inv_0_y [get_bd_pins xup_inv_0/y] [get_bd_pins xup_nand2_1/a] [get_bd_pins xup_nand2_3/a]
connect_bd_net -net xup_inv_1_y [get_bd_pins xup_inv_1/y] [get_bd_pins xup_nand2_2/b] [get_bd_pins xup_nand2_3/b]
connect_bd_net -net xup_nand2_0_y [get_bd_ports AN0] [get_bd_pins xup_nand2_0/y]
connect_bd_net -net xup_nand2_1_y [get_bd_ports AN1] [get_bd_pins xup_nand2_1/y]
connect_bd_net -net xup_nand2_2_y [get_bd_ports AN2] [get_bd_pins xup_nand2_2/y]
connect_bd_net -net xup_nand2_3_y [get_bd_ports AN3] [get_bd_pins xup_nand2_3/y]

regenerate_bd_layout
save_bd_design
generate_target all [get_files ./$project_name.srcs/sources_1/bd/$project_name/$project_name.bd]

make_wrapper -files [get_files ./$project_name.srcs/sources_1/bd/$project_name/$project_name.bd] -top
add_files -norecurse ./$project_name.srcs/sources_1/bd/$project_name/hdl/$project_name\_wrapper.v

update_compile_order -fileset sources_1

launch_runs synth_1 -jobs 2
wait_on_run synth_1

ipx::package_project -root_dir ../../LIBS/MY_LIB/$project_name -vendor xilinx.com -library user -taxonomy /UserIP -module $project_name -import_files -force

close_project
cd ../
