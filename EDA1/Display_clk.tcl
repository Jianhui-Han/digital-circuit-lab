set project_name Display_clk
file mkdir $project_name
cd ./$project_name
create_project -force $project_name ./ -part xc7a35tcpg236-1
set_property target_language verilog [current_project]
set_property simulator_language verilog [current_project]
set_property ip_repo_paths ../../LIBS [current_project]
update_ip_catalog

create_bd_design "$project_name"

set Display_sw_0 [ create_bd_cell -type ip -vlnv xilinx.com:user:Display_sw:1.0 Display_sw_0 ]
set c_counter_binary_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:c_counter_binary:12.0 c_counter_binary_0 ]
set_property -dict [ list CONFIG.Output_Width {2} ] $c_counter_binary_0
set xlslice_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_0 ]
set_property -dict [ list CONFIG.DIN_FROM {0} CONFIG.DIN_TO {0} CONFIG.DIN_WIDTH {2} ] $xlslice_0
set xlslice_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_1 ]
set_property -dict [ list CONFIG.DIN_FROM {1} CONFIG.DIN_TO {1} CONFIG.DIN_WIDTH {2} CONFIG.DOUT_WIDTH {1} ] $xlslice_1
set xup_clk_divider_0 [ create_bd_cell -type ip -vlnv xilinx.com:xup:xup_clk_divider:1.0 xup_clk_divider_0 ]
set_property -dict [ list CONFIG.SIZE {400000} ] $xup_clk_divider_0

set A0 [ create_bd_port -dir I A0 ]
set A1 [ create_bd_port -dir I A1 ]
set A2 [ create_bd_port -dir I A2 ]
set AN0 [ create_bd_port -dir O AN0 ]
set AN1 [ create_bd_port -dir O AN1 ]
set AN2 [ create_bd_port -dir O AN2 ]
set AN3 [ create_bd_port -dir O AN3 ]
set B0 [ create_bd_port -dir I B0 ]
set B1 [ create_bd_port -dir I B1 ]
set B2 [ create_bd_port -dir I B2 ]
set L0 [ create_bd_port -dir O L0 ]
set L1 [ create_bd_port -dir O L1 ]
set L2 [ create_bd_port -dir O L2 ]
set S0 [ create_bd_port -dir I S0 ]
set S1 [ create_bd_port -dir I S1 ]
set S2 [ create_bd_port -dir I S2 ]
set S3 [ create_bd_port -dir I S3 ]
set SIG [ create_bd_port -dir I SIG ]
set SIG_IN_M [ create_bd_port -dir I SIG_IN_M ]
set SIG_IN_N [ create_bd_port -dir I SIG_IN_N ]
set a [ create_bd_port -dir O a ]
set b [ create_bd_port -dir O b ]
set c [ create_bd_port -dir O c ]
set clk [ create_bd_port -dir I clk ]
set d [ create_bd_port -dir O d ]
set e [ create_bd_port -dir O e ]
set f [ create_bd_port -dir O f ]
set g [ create_bd_port -dir O g ]

connect_bd_net -net A0_1 [get_bd_ports A0] [get_bd_pins Display_sw_0/A0]
connect_bd_net -net A1_1 [get_bd_ports A1] [get_bd_pins Display_sw_0/A1]
connect_bd_net -net A2_1 [get_bd_ports A2] [get_bd_pins Display_sw_0/A2]
connect_bd_net -net A3_1 [get_bd_ports SIG_IN_M] [get_bd_pins Display_sw_0/SIG_IN_M]
connect_bd_net -net B0_1 [get_bd_ports B0] [get_bd_pins Display_sw_0/B0]
connect_bd_net -net B1_1 [get_bd_ports B1] [get_bd_pins Display_sw_0/B1]
connect_bd_net -net B2_1 [get_bd_ports B2] [get_bd_pins Display_sw_0/B2]
connect_bd_net -net B3_1 [get_bd_ports SIG_IN_N] [get_bd_pins Display_sw_0/SIG_IN_N]
connect_bd_net -net Display_sw_0_AN0 [get_bd_ports AN0] [get_bd_pins Display_sw_0/AN0]
connect_bd_net -net Display_sw_0_AN1 [get_bd_ports AN1] [get_bd_pins Display_sw_0/AN1]
connect_bd_net -net Display_sw_0_AN2 [get_bd_ports AN2] [get_bd_pins Display_sw_0/AN2]
connect_bd_net -net Display_sw_0_AN3 [get_bd_ports AN3] [get_bd_pins Display_sw_0/AN3]
connect_bd_net -net Display_sw_0_L0 [get_bd_ports L0] [get_bd_pins Display_sw_0/L0]
connect_bd_net -net Display_sw_0_L1 [get_bd_ports L1] [get_bd_pins Display_sw_0/L1]
connect_bd_net -net Display_sw_0_L2 [get_bd_ports L2] [get_bd_pins Display_sw_0/L2]
connect_bd_net -net Display_sw_0_a [get_bd_ports a] [get_bd_pins Display_sw_0/a]
connect_bd_net -net Display_sw_0_b [get_bd_ports b] [get_bd_pins Display_sw_0/b]
connect_bd_net -net Display_sw_0_c [get_bd_ports c] [get_bd_pins Display_sw_0/c]
connect_bd_net -net Display_sw_0_d [get_bd_ports d] [get_bd_pins Display_sw_0/d]
connect_bd_net -net Display_sw_0_e [get_bd_ports e] [get_bd_pins Display_sw_0/e]
connect_bd_net -net Display_sw_0_f [get_bd_ports f] [get_bd_pins Display_sw_0/f]
connect_bd_net -net Display_sw_0_g [get_bd_ports g] [get_bd_pins Display_sw_0/g]
connect_bd_net -net S0_1 [get_bd_ports S0] [get_bd_pins Display_sw_0/S0]
connect_bd_net -net S1_1 [get_bd_ports S1] [get_bd_pins Display_sw_0/S1]
connect_bd_net -net S2_1 [get_bd_ports S2] [get_bd_pins Display_sw_0/S2]
connect_bd_net -net S3_1 [get_bd_ports S3] [get_bd_pins Display_sw_0/S3]
connect_bd_net -net SIG_1 [get_bd_ports SIG] [get_bd_pins Display_sw_0/SIG]
connect_bd_net -net c_counter_binary_0_Q [get_bd_pins c_counter_binary_0/Q] [get_bd_pins xlslice_0/Din] [get_bd_pins xlslice_1/Din]
connect_bd_net -net clkin_1 [get_bd_ports clk] [get_bd_pins xup_clk_divider_0/clkin]
connect_bd_net -net xlslice_0_Dout [get_bd_pins Display_sw_0/SW0] [get_bd_pins xlslice_0/Dout]
connect_bd_net -net xlslice_1_Dout [get_bd_pins Display_sw_0/SW1] [get_bd_pins xlslice_1/Dout]
connect_bd_net -net xup_clk_divider_0_clkout [get_bd_pins c_counter_binary_0/CLK] [get_bd_pins xup_clk_divider_0/clkout]

regenerate_bd_layout
save_bd_design
generate_target all [get_files ./$project_name.srcs/sources_1/bd/$project_name/$project_name.bd]

make_wrapper -files [get_files ./$project_name.srcs/sources_1/bd/$project_name/$project_name.bd] -top
add_files -norecurse ./$project_name.srcs/sources_1/bd/$project_name/hdl/$project_name\_wrapper.v

update_compile_order -fileset sources_1

launch_runs synth_1 -jobs 2
wait_on_run synth_1

ipx::package_project -root_dir ../../LIBS/MY_LIB/$project_name -vendor xilinx.com -library user -taxonomy /UserIP -module $project_name -import_files -force

close_project
cd ../
