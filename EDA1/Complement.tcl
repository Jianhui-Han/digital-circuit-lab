set project_name Complement
file mkdir $project_name
cd ./$project_name
create_project -force $project_name ./ -part xc7a35tcpg236-1
set_property target_language verilog [current_project]
set_property simulator_language verilog [current_project]
set_property ip_repo_paths ../../LIBS [current_project]
update_ip_catalog

create_bd_design "$project_name"

set Adder_4_0 [ create_bd_cell -type ip -vlnv xilinx.com:user:Adder_4:1.0 Adder_4_0 ]
set xlconstant_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_0 ]
set_property -dict [ list CONFIG.CONST_VAL {0} ] $xlconstant_0
set xup_xor2_0 [ create_bd_cell -type ip -vlnv xilinx.com:xup:xup_xor2:1.0 xup_xor2_0 ]
set xup_xor2_1 [ create_bd_cell -type ip -vlnv xilinx.com:xup:xup_xor2:1.0 xup_xor2_1 ]
set xup_xor2_2 [ create_bd_cell -type ip -vlnv xilinx.com:xup:xup_xor2:1.0 xup_xor2_2 ]
set xup_xor2_3 [ create_bd_cell -type ip -vlnv xilinx.com:xup:xup_xor2:1.0 xup_xor2_3 ]

set A0 [ create_bd_port -dir I A0 ]
set A1 [ create_bd_port -dir I A1 ]
set A2 [ create_bd_port -dir I A2 ]
set A3 [ create_bd_port -dir I A3 ]
set A4 [ create_bd_port -dir I A4 ]
set AC0 [ create_bd_port -dir O AC0 ]
set AC1 [ create_bd_port -dir O AC1 ]
set AC2 [ create_bd_port -dir O AC2 ]
set AC3 [ create_bd_port -dir O AC3 ]

connect_bd_net -net A0_1 [get_bd_ports A0] [get_bd_pins xup_xor2_0/a]
connect_bd_net -net A1_1 [get_bd_ports A1] [get_bd_pins xup_xor2_1/a]
connect_bd_net -net A2_1 [get_bd_ports A2] [get_bd_pins xup_xor2_2/a]
connect_bd_net -net A3_1 [get_bd_ports A3] [get_bd_pins xup_xor2_3/a]
connect_bd_net -net A4_1 [get_bd_ports A4] [get_bd_pins Adder_4_0/CI] [get_bd_pins xup_xor2_0/b] [get_bd_pins xup_xor2_1/b] [get_bd_pins xup_xor2_2/b] [get_bd_pins xup_xor2_3/b]
connect_bd_net -net Adder_4_0_S0 [get_bd_ports AC0] [get_bd_pins Adder_4_0/S0]
connect_bd_net -net Adder_4_0_S1 [get_bd_ports AC1] [get_bd_pins Adder_4_0/S1]
connect_bd_net -net Adder_4_0_S2 [get_bd_ports AC2] [get_bd_pins Adder_4_0/S2]
connect_bd_net -net Adder_4_0_S3 [get_bd_ports AC3] [get_bd_pins Adder_4_0/S3]
connect_bd_net -net xlconstant_0_dout [get_bd_pins Adder_4_0/B0] [get_bd_pins Adder_4_0/B1] [get_bd_pins Adder_4_0/B2] [get_bd_pins Adder_4_0/B3] [get_bd_pins xlconstant_0/dout]
connect_bd_net -net xup_xor2_0_y [get_bd_pins Adder_4_0/A0] [get_bd_pins xup_xor2_0/y]
connect_bd_net -net xup_xor2_1_y [get_bd_pins Adder_4_0/A1] [get_bd_pins xup_xor2_1/y]
connect_bd_net -net xup_xor2_2_y [get_bd_pins Adder_4_0/A2] [get_bd_pins xup_xor2_2/y]
connect_bd_net -net xup_xor2_3_y [get_bd_pins Adder_4_0/A3] [get_bd_pins xup_xor2_3/y]

regenerate_bd_layout
save_bd_design
generate_target all [get_files ./$project_name.srcs/sources_1/bd/$project_name/$project_name.bd]

make_wrapper -files [get_files ./$project_name.srcs/sources_1/bd/$project_name/$project_name.bd] -top
add_files -norecurse ./$project_name.srcs/sources_1/bd/$project_name/hdl/$project_name\_wrapper.v

update_compile_order -fileset sources_1

launch_runs synth_1 -jobs 2
wait_on_run synth_1

ipx::package_project -root_dir ../../LIBS/MY_LIB/$project_name -vendor xilinx.com -library user -taxonomy /UserIP -module $project_name -import_files -force

close_project
cd ../
