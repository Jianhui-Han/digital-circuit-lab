`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/07/2017 05:37:49 PM
// Design Name: 
// Module Name: Full_Adder_4_TB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Adder_4_TB();
    reg [3:0] A, B;
    reg CI;
    wire [3:0] S;
    wire CO;
    
    parameter delay_a = 100;
    parameter delay_b = 1600;
    parameter delay_c = 25600;
    
    initial begin
        A = 4'b0000;
        B = 4'b0000;
        CI = 1'b0;
    end
       
    always #delay_a A = A + 1;
    always #delay_b B = B + 1;
    always #delay_c CI = ~CI;
        
    
    Adder_4_wrapper U1
       (.A0(A[0]),
        .A1(A[1]),
        .A2(A[2]),
        .A3(A[3]),
        .B0(B[0]),
        .B1(B[1]),
        .B2(B[2]),
        .B3(B[3]),
        .CI(CI),
        .CO(CO),
        .S0(S[0]),
        .S1(S[1]),
        .S2(S[2]),
        .S3(S[3]));
endmodule
