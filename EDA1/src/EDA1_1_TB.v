`timescale 1ns / 1ps

module EDA1_1_TB();
    reg [3:0] M, N;
    reg [1:0] SW;
    wire [3:0] AN;
    wire L0, a, b, c, d, e, f, g;
    
    parameter delay_m = 400;
    parameter delay_n = 6400;
    parameter delay_sw = 100;
    
    initial begin
        M = 4'b1110;
        N = 4'b0010;
        SW = 2'd0;
    end
    
    always #delay_m M = M + 1;
    always #delay_n N = N + 1;
    always #delay_sw SW = SW + 1;

    EDA1_1_wrapper inst(
		.AN0(AN[0]),
        .AN1(AN[1]),
        .AN2(AN[2]),
        .AN3(AN[3]),
        .L0(L0),
        .M0(M[0]),
        .M1(M[1]),
        .M2(M[2]),
        .M3(M[3]),
        .N0(N[0]),
        .N1(N[1]),
        .N2(N[2]),
        .N3(N[3]),
        .SW0(SW[0]),
        .SW1(SW[1]),
        .a(a),
        .b(b),
        .c(c),
        .d(d),
        .e(e),
        .f(f),
        .g(g)
		);

endmodule
