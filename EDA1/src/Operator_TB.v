`timescale 1ns / 1ps

module Operator_TB();

    reg [3:0] M, N;
    wire [3:0] S;
    wire SIG, SIG_IN_M, SIG_IN_N;
    
    parameter delay_m = 100;
    parameter delay_n = 1600;
    
    initial begin
        M = 4'b0000; N = 4'b0000;
    end
    
    always #delay_m M = M + 1;
    always #delay_n N = N + 1;

    Operator_wrapper inst
       (.M0(M[0]),
        .M1(M[1]),
        .M2(M[2]),
        .M3(M[3]),
        .N0(N[0]),
        .N1(N[1]),
        .N2(N[2]),
        .N3(N[3]),
        .S0(S[0]),
        .S1(S[1]),
        .S2(S[2]),
        .S3(S[3]),
        .SIG(SIG),
        .SIG_IN_M(SIG_IN_M),
        .SIG_IN_N(SIG_IN_N));
endmodule
