`timescale 1ns / 1ps

module Adder_1_TB();
    reg A, B, CI;
    wire S, CO;
    
    initial begin
        A = 0; B = 0; CI = 0;
        #100 A = 0; B = 0; CI = 1;
        #100 A = 0; B = 1; CI = 0;
        #100 A = 0; B = 1; CI = 1;
        #100 A = 1; B = 0; CI = 0;
        #100 A = 1; B = 0; CI = 1;
        #100 A = 1; B = 1; CI = 0;
        #100 A = 1; B = 1; CI = 1;
    end

    Adder_1_wrapper inst
       (.A(A),
        .B(B),
        .CI(CI),
        .CO(CO),
        .S(S));
endmodule
