set project_name EDA1_1
file mkdir $project_name
cd ./$project_name
create_project -force $project_name ./ -part xc7a35tcpg236-1
set_property target_language verilog [current_project]
set_property simulator_language verilog [current_project]
set_property ip_repo_paths ../../LIBS [current_project]
update_ip_catalog

create_bd_design "$project_name"

set Display_sw_0 [ create_bd_cell -type ip -vlnv xilinx.com:user:Display_sw:1.0 Display_sw_0 ]
set Operator_0 [ create_bd_cell -type ip -vlnv xilinx.com:user:Operator:1.0 Operator_0 ]

set AN0 [ create_bd_port -dir O AN0 ]
set AN1 [ create_bd_port -dir O AN1 ]
set AN2 [ create_bd_port -dir O AN2 ]
set AN3 [ create_bd_port -dir O AN3 ]
set L0 [ create_bd_port -dir O L0 ]
set L1 [ create_bd_port -dir O L1 ]
set L2 [ create_bd_port -dir O L2 ]
set M0 [ create_bd_port -dir I M0 ]
set M1 [ create_bd_port -dir I M1 ]
set M2 [ create_bd_port -dir I M2 ]
set M3 [ create_bd_port -dir I M3 ]
set N0 [ create_bd_port -dir I N0 ]
set N1 [ create_bd_port -dir I N1 ]
set N2 [ create_bd_port -dir I N2 ]
set N3 [ create_bd_port -dir I N3 ]
set SW0 [ create_bd_port -dir I SW0 ]
set SW1 [ create_bd_port -dir I SW1 ]
set a [ create_bd_port -dir O a ]
set b [ create_bd_port -dir O b ]
set c [ create_bd_port -dir O c ]
set d [ create_bd_port -dir O d ]
set e [ create_bd_port -dir O e ]
set f [ create_bd_port -dir O f ]
set g [ create_bd_port -dir O g ]

connect_bd_net -net Display_sw_0_AN0 [get_bd_ports AN0] [get_bd_pins Display_sw_0/AN0]
connect_bd_net -net Display_sw_0_AN1 [get_bd_ports AN1] [get_bd_pins Display_sw_0/AN1]
connect_bd_net -net Display_sw_0_AN2 [get_bd_ports AN2] [get_bd_pins Display_sw_0/AN2]
connect_bd_net -net Display_sw_0_AN3 [get_bd_ports AN3] [get_bd_pins Display_sw_0/AN3]
connect_bd_net -net Display_sw_0_L0 [get_bd_ports L0] [get_bd_pins Display_sw_0/L0]
connect_bd_net -net Display_sw_0_L1 [get_bd_ports L1] [get_bd_pins Display_sw_0/L1]
connect_bd_net -net Display_sw_0_L2 [get_bd_ports L2] [get_bd_pins Display_sw_0/L2]
connect_bd_net -net Display_sw_0_a [get_bd_ports a] [get_bd_pins Display_sw_0/a]
connect_bd_net -net Display_sw_0_b [get_bd_ports b] [get_bd_pins Display_sw_0/b]
connect_bd_net -net Display_sw_0_c [get_bd_ports c] [get_bd_pins Display_sw_0/c]
connect_bd_net -net Display_sw_0_d [get_bd_ports d] [get_bd_pins Display_sw_0/d]
connect_bd_net -net Display_sw_0_e [get_bd_ports e] [get_bd_pins Display_sw_0/e]
connect_bd_net -net Display_sw_0_f [get_bd_ports f] [get_bd_pins Display_sw_0/f]
connect_bd_net -net Display_sw_0_g [get_bd_ports g] [get_bd_pins Display_sw_0/g]
connect_bd_net -net Operator_0_S0 [get_bd_pins Display_sw_0/S0] [get_bd_pins Operator_0/S0]
connect_bd_net -net Operator_0_S1 [get_bd_pins Display_sw_0/S1] [get_bd_pins Operator_0/S1]
connect_bd_net -net Operator_0_S2 [get_bd_pins Display_sw_0/S2] [get_bd_pins Operator_0/S2]
connect_bd_net -net Operator_0_S3 [get_bd_pins Display_sw_0/S3] [get_bd_pins Operator_0/S3]
connect_bd_net -net Operator_0_SIG [get_bd_pins Display_sw_0/SIG] [get_bd_pins Operator_0/SIG]
connect_bd_net -net Operator_0_SIG_IN_M [get_bd_pins Display_sw_0/SIG_IN_M] [get_bd_pins Operator_0/SIG_IN_M]
connect_bd_net -net Operator_0_SIG_IN_N [get_bd_pins Display_sw_0/SIG_IN_N] [get_bd_pins Operator_0/SIG_IN_N]
connect_bd_net -net M0_1 [get_bd_ports M0] [get_bd_pins Display_sw_0/A0] [get_bd_pins Operator_0/M0]
connect_bd_net -net M1_1 [get_bd_ports M1] [get_bd_pins Display_sw_0/A1] [get_bd_pins Operator_0/M1]
connect_bd_net -net M2_1 [get_bd_ports M2] [get_bd_pins Display_sw_0/A2] [get_bd_pins Operator_0/M2]
connect_bd_net -net M3_1 [get_bd_ports M3] [get_bd_pins Operator_0/M3]
connect_bd_net -net N0_1 [get_bd_ports N0] [get_bd_pins Display_sw_0/B0] [get_bd_pins Operator_0/N0]
connect_bd_net -net N1_1 [get_bd_ports N1] [get_bd_pins Display_sw_0/B1] [get_bd_pins Operator_0/N1]
connect_bd_net -net N2_1 [get_bd_ports N2] [get_bd_pins Display_sw_0/B2] [get_bd_pins Operator_0/N2]
connect_bd_net -net N3_1 [get_bd_ports N3] [get_bd_pins Operator_0/N3]
connect_bd_net -net SW0_1 [get_bd_ports SW0] [get_bd_pins Display_sw_0/SW0]
connect_bd_net -net SW1_1 [get_bd_ports SW1] [get_bd_pins Display_sw_0/SW1]

regenerate_bd_layout
save_bd_design
generate_target all [get_files ./$project_name.srcs/sources_1/bd/$project_name/$project_name.bd]

make_wrapper -files [get_files ./$project_name.srcs/sources_1/bd/$project_name/$project_name.bd] -top
add_files -norecurse ./$project_name.srcs/sources_1/bd/$project_name/hdl/$project_name\_wrapper.v

add_files -fileset sim_1 -norecurse -copy_to ./$project_name.srcs/sim_1/new ../src/$project_name\_TB.v

add_files -fileset constrs_1 -norecurse -copy_to ./$project_name.srcs/constrs_1/new ../src/$project_name.xdc

update_compile_order -fileset sources_1
update_compile_order -fileset sim_1

launch_runs synth_1  -jobs 2
wait_on_run synth_1

launch_runs impl_1 -to_step write_bitstream -jobs 2
wait_on_run impl_1

close_project
cd ../
